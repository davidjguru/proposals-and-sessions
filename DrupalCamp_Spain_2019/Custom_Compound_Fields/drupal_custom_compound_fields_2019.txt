Title: Custom Compound Fields en Drupal (tal vez) la solución que estabas buscando
Subtitle: Teoría y práctica de campos compuestos en Drupal
Author: David Rodríguez, @davidjguru
Context: DrupalCamp Spain, Conil,  Andalusia, 2019
Proposal: Session
Level: Basic
Language: Spanish, Castilian
Timing: 45'

----------------------------------------------------------------
Una de las claves fundamentales de la construcción de un proyecto es la de establecer los mecanismos de entrada de datos: definir que input necesitamos, que tipos básicos manejaremos y sobre todo, como articularemos la información. Ahí cobra importancia conocer la construcción de campos custom en Drupal. Pero podemos encontrarnos en un paso más allá con la necesidad de crear unidades de información algo más complejas, los denominados Campos Compuestos o Custom Compound Fields en Drupal.

En esta sesión repasaremos en que se fundamentan los CCF en Drupal, así como sus características más importantes, como se construyen y además, que diferencias pueden existir respecto a otras soluciones con fines parecido.

Veremos especialmente cuando más allá de un sitio web estamos trabajando en una Herramienta de gestión basada en Drupal 8 que requiere campos articulados con ciertas particularidades, para lo que visitaremos un caso específico y real de uso que permitirá identificar claramente todo lo expuesto, y casaremos fielmente la teoría con la práctica.

----------------------------------------------------------------


**Keywords:** Drupal, Form API, Form, Custom Field, Scheme, PHP, Composer, Git.
